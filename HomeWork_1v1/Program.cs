﻿using System;

namespace HomeWork_1v1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Игра

            Random rand = new Random();
            string name1 = "First"; // Имя первого игрока
            string name2 = "Second"; //Имя второго игрока

            int MaxHealth = 30; // Максимальное количество жизней у чудовища при рандомизации
            int MinHealth = 10; // Минимальное количество жизней чудовища при рандомизации
            int n = 1;          // Единица урона у Игроков
            int maxValue = 4; // Максимальнодопустимая единица урона
            int minValue = 0; // Минимальная единица урона, если добавить один
            int revival = rand.Next(MinHealth, MaxHealth);
            int man1 = 0; //ход первого игрока
            int man2 = 0; // ход второго игрока или компьютера
            int level = 0; // уровень сложности
            string number = "1,2,3,4"; // варианты урона
            
            Console.WriteLine("Выберите уровень сложности от 1 до 3:");
            level = int.Parse(Console.ReadLine());
            

            switch(level)
            {
                case 1: Console.WriteLine("Вы выбрали легкий уровень!");
                    Console.WriteLine("Вы можете использовать эти числа");
                    for (int i = minValue+1; i < maxValue+1; i++)
                    {
                        Console.Write($" {i}");
                       
                    }
                    break;
                case 2: Console.WriteLine("Вы выбрали средний уровень!");
                    Console.WriteLine("Вы можете использовать эти числа");
                    minValue += 1;
                    maxValue += 3;
                    MinHealth = 30;
                    MaxHealth = 50;
                    number = "2,3,4,5,6,7";
                    for (int i = minValue + 1; i < maxValue + 1; i++)
                    {
                        Console.Write($" {i}");

                    }
                    break;
                case 3: Console.WriteLine("Вы выбрали тяжелый уровень!");
                    Console.WriteLine("Вы можете использовать эти числа");
                    minValue = 5;
                    maxValue = 10;
                    MinHealth = 50;
                    MaxHealth = 100;
                    number = "5,6,7,8,9,10";
                    for (int i = minValue;i< maxValue + 1;i++)
                    {
                        Console.Write($"{i} ");
                    }
                    break;
                default: Console.WriteLine("Такого уровня пока не существует - поэтому вы будете играть на легком уровне!");
                    break;
            }
            int game = 0;
            Console.WriteLine("Выберите режим игры. Если Игрок 1 против Игрок 2 - нажмите 1. Если Игрок против Компьютера нажмите 2.");
            game = int.Parse(Console.ReadLine());
            bool duel = game == 1 || game > 2 || game < 0;
            for (; ; )
                if (duel)
                {
                    Console.WriteLine("\nВведите имя Первого игрока:");
                    name1 = Console.ReadLine();
                    Console.WriteLine("Введите имя Второго игрока:");
                    name2 = Console.ReadLine();
                    int coin = rand.Next(2); //Монета выбирает, кто ходит первый


                    if (coin > 0)
                    {
                        Console.WriteLine("\nПервым ходит:" + name1);
                        man1 = 1;
                        man2 = 0;
                    }
                    else
                    {
                        Console.WriteLine("Первым ходит:" + name2);
                        man2 = 1;
                        man1 = 0;
                    }
                    int boss = rand.Next(MinHealth, MaxHealth);
                    for (; ; )
                    {
                        if (boss > 0)
                        {
                            if (man1 > man2)
                            {
                                Console.WriteLine($"Чудовище имеет {boss} жизней");
                                Console.WriteLine($"{name1} введите число({number}):");
                                n = int.Parse(Console.ReadLine());
                                if (n > maxValue)
                                {
                                    Console.WriteLine($"{name1} - Вы совершили промах!");
                                    Console.WriteLine("Попробуйте еще:");
                                    n = int.Parse(Console.ReadLine());
                                }


                                if (n < minValue)
                                {
                                    Console.WriteLine($"{name1} - Хватит лечить чудовище,это бесполезно!");
                                    Console.WriteLine("Попробуйте еще:");
                                    n = int.Parse(Console.ReadLine());
                                }
                                boss -= n;
                                man2 += 2;
                            }
                            else
                            {
                                if (man2 > man1)
                                {
                                    Console.WriteLine($"Чудовище имеет {boss} жизней");
                                    Console.WriteLine($"{name2} введите число({number}):");
                                    n = int.Parse(Console.ReadLine());
                                    if (n > maxValue)
                                    {
                                        Console.WriteLine($"{name2} - Вы совершили промах!");
                                        Console.WriteLine("Попробуйте еще:");
                                        n = int.Parse(Console.ReadLine());
                                    }

                                    if (n < minValue)
                                    {
                                        Console.WriteLine($"{name2} - Хватит лечить чудовище, это бесполезно!");
                                        Console.WriteLine("Попробуйте еще:");
                                        n = int.Parse(Console.ReadLine());
                                    }
                                    boss -= n;
                                    man1 += 2;
                                }
                            }


                        }

                        if (boss == 0)
                        {
                            if (man1 > man2)
                            {
                                Console.WriteLine($"{name2} Вы победили чудовище!");
                                Console.ReadKey();
                            }
                            else
                            {
                               
                                
                                 Console.WriteLine($"{name1} Вы победили чудовище!");
                                 Console.ReadKey();
                                
                            }
                        }

                        if (boss < 0)
                        {

                            Console.WriteLine("Вы так сильно ударили чудовище, что Бог сжалился над ним и решил подлечить его раны:");
                            boss += revival;
                        }
                    }
                }
                else
                {
                    if (game == 2)
                    {
                        int nick = rand.Next(1, 10); //Выбор первого имени
                        int name = rand.Next(1, 10); //Выбор второго имени
                        string name3 = "враг"; //Имя компьютера
                        string Firstname = "Определение"; //Первое имя
                        string Lastname = "Существительное"; //Второе имя
                        Console.WriteLine("Введите свое имя:");
                        name1 = Console.ReadLine();

                        switch (nick)
                        {
                            case 1: Firstname = "Однорукий"; break;
                            case 2: Firstname = " Безбашенный"; break;
                            case 3: Firstname = " Отвратительный"; break;
                            case 4: Firstname = " Хромой"; break;
                            case 5: Firstname = " Ужасный"; break;
                            case 6: Firstname = " Независимый"; break;
                            case 7: Firstname = "Адский"; break;
                            case 8: Firstname = "Могучий"; break;
                            case 9: Firstname = "Ленивый"; break;
                        }

                        switch (name)
                        {
                            case 1: Lastname = "Бандит"; break;
                            case 2: Lastname = "Пират"; break;
                            case 3: Lastname = "Доходяга"; break;
                            case 4: Lastname = "Головорез"; break;
                            case 5: Lastname = "Рыцарь"; break;
                            case 6: Lastname = "Ковбой"; break;
                            case 7: Lastname = "Пьяница"; break;
                            case 8: Lastname = "Маньяк"; break;
                            case 9: Lastname = "Предатель"; break;
                        }
                        name3 = Firstname + " " + Lastname;
                        Console.WriteLine($"Имя Вашего соперника: {name3}");

                        int coin = rand.Next(2);


                        if (coin > 0)
                        {
                            Console.WriteLine("\nПервым ходит:" + name1);
                            man1 = 1;
                            man2 = 0;
                        }
                        else
                        {
                            Console.WriteLine("Первым ходит:" + name3);
                            man2 = 1;
                            man1 = 0;
                        }
                        int boss = rand.Next(MinHealth, MaxHealth);

                        for (; ; )
                        {
                            if (boss > 0)
                            {
                                if (man1 > man2)
                                {
                                    Console.WriteLine($"Чудовище имеет {boss} жизней");
                                    Console.WriteLine($"{name1} введите число({number}):");
                                    n = int.Parse(Console.ReadLine());
                                    if (n > maxValue)
                                    {
                                        Console.WriteLine($"{name1} - Вы совершили промах!");
                                        Console.WriteLine("Попробуйте еще:");
                                        n = int.Parse(Console.ReadLine());
                                    }


                                    if (n < minValue)
                                    {
                                        Console.WriteLine($"{name1} - Хватит лечить чудовище,это бесполезно!");
                                        Console.WriteLine("Попробуйте еще:");
                                        n = int.Parse(Console.ReadLine());
                                    }
                                    boss -= n;
                                    man2 += 2;
                                }
                                else
                                {
                                    if (man2 > man1)
                                    {
                                        Console.WriteLine($"Чудовище имеет {boss} жизней");
                                        if (boss > MinHealth / 2)
                                        {
                                            Console.Write($"{name3} нанес чудовищу {maxValue} урона!");
                                            boss -= maxValue;
                                            man1 += 2;
                                        }
                                        else
                                        {
                                            if (boss < MinHealth / 2 && boss > maxValue)
                                            {
                                                Console.Write($"{name3} нанес чудовищу {maxValue - 1} урона!");
                                                boss = boss - (maxValue - 1);
                                                man1 += 2;
                                            }
                                            else
                                            {
                                                if (boss < maxValue && boss > minValue)
                                                {
                                                    Console.Write($"{name3} нанес чудовищу {boss} урона!");
                                                    boss -= boss;
                                                    man1 += 2;
                                                }
                                                else
                                                {


                                                    Console.Write($"{name3} нанес чудовищу {maxValue} урона!");
                                                    boss -= maxValue;
                                                    man1 += 2;


                                                }
                                            }
                                        }
                                    }
                                }



                            }
                            else
                            {

                                if (boss == 0)
                                {
                                    if (man1 > man2) 
                                    {
                                        Console.Write($" {name3} Вы победили чудовище!");
                                        Console.ReadKey();
                                    }
                                    else
                                    {

                                        Console.WriteLine($"{name1} Вы победили чудовище!");
                                        Console.ReadKey();

                                    }
                                }

                                else
                                {

                                    Console.WriteLine("Вы так сильно ударили чудовище, что Бог сжалился над ним и решил подлечить его раны:");
                                    boss += revival;
                                }
                            }
                        }
                    }
                }
        }
    }
}
